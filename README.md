# Morse Code Tutor Project

Welcome to the Morse Code Tutor Project, developed by the Microprocessor Systems Group. This project aims to create an interactive game that teaches Morse Code using C language and ARM assembly. The game is designed for the MAKER-PI-PICO board and offers multiple levels of difficulty to engage players.

## Overview

The Morse Code Tutor Project focuses on implementing a game where players translate alphanumeric characters and words into Morse Code by pressing buttons. This README provides an overview of the project, including its objectives, workflow, GitLab usage, and code structure.

## Workflow

Tasks were broken down into manageable issues and tracked through milestones on GitLab. Each issue was assigned to team members based on their strengths and preferences. Noteworthy tasks included creating initial templates, implementing GPIO button interrupts, managing LED color control, and developing the game's logic.

## GitLab

The project repository was established on GitLab, where tasks were organized into milestones and assigned to team members. GitLab served as a central hub for collaboration, enabling smooth integration of code contributions and effective project management.

## Code

The code implements the game's functionality, including button input processing, LED color control, and game progression logic. Written in C and ARM assembly, the code utilizes Raspberry Pi Pico SDK libraries for hardware interaction. Detailed explanations of functions and their implementations are provided in both the `.c` and `.S` files.

## Conclusion

The Morse Code Tutor Project represents a collaborative effort to develop an educational game using C and ARM assembly. Leveraging GitLab for project management and version control ensured seamless collaboration and code integration. We invite you to explore our project repository and contribute to the advancement of Morse Code learning through interactive gameplay.

For further inquiries or collaboration opportunities, feel free to contact the project team members listed in the project documentation.

Thank you for your interest in our project!
