#include<stdio.h>

char* int_to_char(int index) {
    if (index >= 1 && index <= 26) {
        return (char[]){index + 'A' - 1, '\0'};
    } else if (index >= 27 && index <= 36) {
        return (char[]){index + '0' - 27, '\0'};
    } else {
        return "?";
    }
}

int main()
{
    char*p;
    for(int i=1;i<=36;i++)
    {
        printf("int: %d, char: %c\n",i,*int_to_char(i));
    }
    return 0;
}